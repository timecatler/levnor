# levnor
Level normalization utility written in Haskell.
## Features
Takes image and transforms luminance to a normal distribution.
Works with grayscale images.

Grayscale input
![Grayscale in](./images/examples/grayscale.jpg)

Grayscale output
![Grayscale out](./images/examples/grayscale_out.jpg)

Also works with colored images.

Colored input
![Colored in](./images/examples/colored.jpg)

Colored output
![Colored out good](./images/examples/colored_out_good.jpg)

And some distribution parameters work worse than others

Colored output bad
![Colored out bad](./images/examples/colored_out_bad.jpg)

## Requirements
* Stack

## Building
```shell-script
git clone https://github.com/timecatler/levnor.git
cd levnor
stack install
```

Stack binaries location should be on your `PATH` for that to work.

## Usage

`levnor ./path/to/input/image.jpeg ./path/to/output/image.bmp median standart_deviation`